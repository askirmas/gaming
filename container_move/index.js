function main() {
  let top = 0, left = 0, inDrag = false
  const container = document.getElementsByClassName('semicircle')[0]
  , point = document.getElementsByClassName('circle')[0]
  , end = document.getElementsByClassName('end')[0].getBoundingClientRect()
  , reset = (succeeded = false) => {
    inDrag = false
    if (succeeded !== true) {
      point.style.left = '0px'
      point.style.top = '0px'
    }
  }
  , containerEvents = {
    "mouseout": ({clientX, clientY}) => {
      if (!inDrag)
        return;
      const classNames = [...document.elementFromPoint(clientX, clientY).classList]
      if (!classNames.some(x => ['semicircle', 'circle', 'end'].includes(x)))
        reset()
    },
    "mousedown": ({target, clientX, clientY}) => {
      if (target !== point)
        return
      top = clientY
      left = clientX
      inDrag = true
    },
    "mouseup": () => reset(),
    "mousemove": ({clientX, clientY}) => {
      if (!inDrag)
        return;
      point.style.left = `${clientX - left}px`
      point.style.top = `${clientY - top}px`
      if (
        clientX >= end.left
        && clientX <= end.right
        && clientY >= end.top
        && clientY <= end.bottom
      ) {
        reset(true)
        alert('success')
      }
        
    }
  }
  
  Object.entries(containerEvents)
  .forEach(([evName, handler]) =>
    container.addEventListener(evName, handler, true) 
  )
}

