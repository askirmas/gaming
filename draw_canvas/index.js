function main(){
  const canvas = document.getElementsByTagName('canvas')[0]
  , ctx = canvas.getContext("2d", {alpha: false})
  , radius = 20
  , {width, height} = canvas.getBoundingClientRect()

  Object.assign(canvas, {width, height})
  
  const fn = ({which, clientX, clientY}) => {
    if (which !== 1)
      return;
    const {left, top} = canvas.getBoundingClientRect()

    ctx.fillStyle = "#FF0000";
    ctx.beginPath();
    //ctx.ellipse(clientX - left, clientY - top, radius, radius, 0, 0, 2 * Math.PI);
    ctx.arc(clientX - left, clientY - top, radius, 0, 2 * Math.PI);
    ctx.fill()
  }

  canvas.addEventListener("mousedown", ev => {
    canvas.addEventListener("mousemove", fn)
  })
  canvas.addEventListener("mouseup", ev => {
    canvas.removeEventListener("mousemove", fn)
  })
}