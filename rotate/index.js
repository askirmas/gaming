class CssClass {
  constructor(str) {
    this.toString = () => str
    this.valueOf = () => `.${str}`
  }
}
function $() {
  const args = [...arguments]
  , selector = args.pop()
  , [scope = document] = args 
  return scope.querySelectorAll(selector.valueOf())
}
const attributes = {
  group: 'group'
}
, grouping = group => `[data-${attributes.group}="${group}"]`
, classes = {
  element:  new CssClass('el'),
  selected: new CssClass('selected'),
  solved: new CssClass('solved'),
}
/*, rules= {
  solvedCount: 2
//TODO: , elementsCount | groupsCount
}
, data = [4, 4]*/
, {rules, data} = [...new URLSearchParams(window.location.search)]
.reduce((acc, [key, value]) => {
  try {
    acc[key] = JSON.parse(value)
  } catch(e) {
    acc[key] = value
  }
  return acc
}, {})

function render(root, fn) {
  return window.requestAnimationFrame(() => root.append(...fn(window.getComputedStyle(root))))
}

function main(rootStyles) {
  const animation = {
    showTime: parseInt(rootStyles.getPropertyValue('--show-time'))
    , duration: parseInt(rootStyles.getPropertyValue('--duration'))
  }
  , children = []
  , values = Array.isArray(data) ? data : Object.values(data)

  for (let group = values.length; group--;) {
    const {count, audio} = typeof values[group] === "number"
    ? {count: values[group]}
    : values[group]

    for (let item = count; item--;) {
      const el = document.createElement('div')
      , handler = onSelect.bind(el, animation)

      el.classList.add(classes.element)
      Object.assign(el.dataset, {[attributes.group]: group, item})
      Object.assign(el.style, {'--order': Math.trunc(Math.random() * 1000000)})
      el.style.setProperty('--order', Math.trunc(Math.random() * 1000000))
      el.addEventListener('click', handler)
      if (audio) {
        el.append(...domCreateElement(
          'audio',
          undefined,
          [
            {
              element: 'source',
              props: {
                "src": audio,
                "type": "audio/mpeg"
              }
            } 
          ]
        ))
      }
      children.push(el)
    }
  }

  return children
}

function onSelect({showTime, duration}) {
  const {classList, dataset: {[attributes.group]: group}} = this
  if (classList.contains(classes.selected))
    return;
  classList.add(classes.selected)
  
  const audio = $(this, 'audio')[0]
  if (audio) {
    try {
      audio.load()
      audio.play()
    } catch(e) {}
  }

  if (isOtherGroupSelected(group))
    return setTimeout(
      unselectAll,
      showTime + duration
    )

  if (isEnoughSelected(group, rules.solvedCount))
    selected2solved()

  if (areAllSolved())
    setTimeout(
      celebration,
      duration
    )
}

function unselectAll() {
  for (let el of $(classes.selected))
    el.classList.remove(classes.selected)
}
function selected2solved() {
  for (let el of $(classes.selected)) {
    el.classList.remove(classes.selected)
    el.classList.add(classes.solved)
  }
}
function celebration() {
  alert('+')
  return window.location.reload()
}
function isOtherGroupSelected(group) {
  return $(classes.element + classes.selected + not(grouping(group)))
  .length > 0
}
function isEnoughSelected(group, count) {
  return count
  && $(classes.element + classes.selected)
  .length === count
  || $(classes.element + not(classes.selected) + grouping(group))
  .length === 0
}
function areAllSolved() {
  return $(classes.element + not(classes.solved))
  .length === 0
}

function not(str) {
  return ':not(' + str + ')'
}

function domCreateElement(element, props, children) {
  const [el, p, ch] = (typeof element === 'function')
  ? element(props, children)
  : arguments

  const childrenRendered = []
  if (ch)
    for (let child of ch) {
      if (['string', 'number'].includes(typeof child))
        childrenRendered.push(document.createTextNode(child))
      else if (children !== null && typeof children === 'object') {
        const {element, props, children} = child
        childrenRendered.push(...domCreateElement(element, props, children))
      }
    }
  if (!el)
    return childrenRendered
    
  const domElement = document.createElement(el)
  if (childrenRendered.length > 0)
    domElement.append(...childrenRendered)
  
  if (p)
    for (let prop in p)
      switch(prop) {
        case 'style':
        case 'dataset':
          Object.assign(domElement[prop], p[prop])
          break;
        default:
          domElement.setAttribute(prop, p[prop])
      }
  return [domElement]
}